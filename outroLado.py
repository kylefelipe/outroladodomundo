"""
Calcula o ponto mais distante de uma localização.
"""
import math
from operator import mod

def reverseLat(latitude: float) -> float:
    """Calculta o inverso da latitude, não pode ser maior que 90."""
    return latitude * -1

def reverseLong(longitude: float) -> float:
    """Não pode ser maior que 180."""
    
    revLong = 180 - abs(longitude)
    
    if longitude > 0:
        revLong * -1
    
    return revLong

def outroLado(longitude: float, latitude: float) -> list:
    """Calcula o ponto mais distante."""
    otLado = reverseLong(longitude), reverseLat(longitude)
    
    return otLado
