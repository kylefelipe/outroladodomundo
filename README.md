# O outro lado do mundo

A idéia dessa função nasceu após assistir a um vídeo do canal mais sensacional de Ciência do YouTube, o [Manual do Mundo](http://www.manualdomundo.com.br/) onde o Iberê Tenório explica como se calcula o ponto mais distante do globo de uma determinada coordenada.  
Ou seja, o outro lado do mundo.

Esse video foi publicado no vlog do canal a mais de um ano, e só agora consegui começar a fazer o código.

[Confira o vídeo aqui.](https://www.youtube.com/watch?v=DVbP5_FCyRw)

O calculo é feito utilizando coordenadas geográficas, no formato GRAU DECIMAL.  

O calculo é feito da seguinte forma, conforme explicado por ele no canal.  

>latitude contrária = latitude * -1
>
>longitude contrária = 180 - |longitude|
>
>se longitude > 0: longitude contrária * -1

## Detalhes

Indicação de Hemisfério deve ser sempre a presença do sinal '-'.

Longitude: Deverá estar sempre entre -180 e 180

Latitude: Deverá estar sempre entre -90 e 90.

> Mas no vídeo, o Iberê usa latitude primeiro e no código você usa longitude primeiro.

Como eu estou montado o código de uma forma que possa ser utilizado em softwares que trabalham com dados geográficos, por padrão as coordenadas nesses softwares são no padrão Longitude, Latitude. sendo assim, basta inverter o par de coordenadas ou usar as funções separadas.

## Objetivo

Aplicar as regras da PEP do Python.  
Utilizar as formas de documentação de código.  
Pegar as coordenadas em qualquer sistema de coordenadas e utilizar em coordenadas geográficas no datum desejado.  
Transformar o em um plugin que faça o cálculo da posição contrária dos vetores no Qgis.  
